import ast
from django import template

register = template.Library()


@register.filter
def test_score(scores, type):
    for score in scores:
        if score.type_id == type:
            return '%i%%' % score.score
    return 'No Data'


@register.filter
def downloaded_pg(downloads, delivery):
    for download in downloads.select_related():
        if download.document.course == delivery.course:
            return True
    return False


@register.filter
def request_contact_info(request, number):
    number = int(number)
    data = {}
    try:
        data['agency'] = request.data['agency%i' % number]
    except KeyError:
        data['agency'] = None
    try:
        data['contact'] = request.data['contact_name%i' % number]
    except KeyError:
        data['contact'] = None
    try:
        data['phone'] = request.data['phone%i' % number]
    except KeyError:
        data['phone'] = None
    try:
        data['cell'] = request.data['cell%i' % number]
    except KeyError:
        data['cell'] = None
    try:
        data['fax'] = request.data['fax%i' % number]
    except KeyError:
        data['fax'] = None
    try:
        data['email'] = request.data['email%i' % number]
    except KeyError:
        data['email'] = None
    return data
