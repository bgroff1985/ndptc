class DeliveryStatus:
    NONE            = 0x0
    COMPLETE        = 0x1  # Obsolete
    PENDING         = 0x2  # Obsolete
    CODE_REQUIRED   = 0x4
    PRE_TEST        = 0x8
    POST_TEST       = 0x10
    EVALUATION      = 0x20
    ONLINE_REGISTRATION = 0x40
    AUTO_COMPUTE_DATES  = 0x80  # Obsolete
    WAIT_LIST           = 0x100  # Obsolete


EVALUATION_QUESTIONS = (
    ('1.  BEFORE the course, I would rate my knowledge, skills and abilities as:'),
    ('2.  AFTER the course, I would rate my knowledge, skills and abilities as:'),
    ('3.  The course content supported the learning objectives.'),
    ('4.  The course materials and learning aids effectively conveyed the course content.'),
    ('5.  The time allocated to accomplish the course objectives was appropriate.\n' +
     '(If too long or too short, please explain in Question 26).'),
    ('6.  The course contained useful activities to practice and reinforce the learning objectives.'),
    ('7.  The instructor(s) were prepared for the class.'),
    ('8.  The instructor(s) demonstrated thorough knowledge of the course content.'),
    ('9.  The instructor(s) were able to answer questions clearly and understandably.'),
    ('10.  The instructor(s) conducted the training in a skilled and competent manner.'),
    ('11.  The instructor(s) encouraged student participation.'),
    ('12.  The instructor(s) made the course objectives and expectations clear to the students.'),
    ('13.  The instructor(s) used instructional time effectively.'),
    ('14.  The instructor(s) covered all of the course learning objectives.'),
    ('15.  The instructor(s) used multiple learning strategies and techniques.'),
    ('16.  The course was relevant to the knowledge and skills I need to accomplish the job for which I am receiving this training.'),
    ('17.  The practical exercises enhanced learning of course content. (Respond only if course included practical exercises).'),
    ('18.  The course content was appropriate for someone in my professional field.'),
    ('19.  The course content was appropriate for someone with my level of experience.'),
    ('20.  Overall, the course content met my needs and expectations.'),
    ('21.  Overall, the instructors'' performance met my needs and expectations.'),
    ('22.  Overall, the course increased my knowledge, skills and abilities relevant to the course topics.'),
    ('23.  I would recommend this course to my peers.'),
    ('24.  Which part(s) of the course was (were) MOST valuable to you? Please explain why.'),
    ('25.  Which part(s) of the course was (were) LEAST valuable to you? Please explain why.'),
    ('26.  Please provide any other comments or suggestions you have for improving the course.'),
    ('27.  What other training is most important to you now that you have completed this course?'),
)

EVALUATION_SECTIONS = (
    ('Knowledge/Skills/Abilities (KSAs) Level in Subject Matter'),
    ('Course Evaluation'),
    ('Instructor Evaluation'),
    ('Course Benefit'),
    ('Overall Ratings'),
    ('General Comments'),
)

INSTRUCTOR_EVALUATION_QUESTIONS = (
    ('1. The instructor presented him/herself in a professional manner.'),
    ('2. The instructor spoke clearly - using appropriate tone and volume.'),
    ('3. The instructor used words and terms that were appropriate to my level and understanding.'),
    ('4. The instructor remained focused on course materials and did not deviate into personal stories or anecdotes.'),
    ('5. The instructor expresses course concepts in a clear and concise manner.'),
    ('6. The instructor explained the exercises well and provided beneficial feedback.'),
    ('Please feel free to include any additional comments about the organization of the course or the effectiveness of the instructor:')
)

SURVEY_CHOICES = (
    (5, '5 - Strongly Agree'),
    (4, '4 - Agree'),
    (3, '3 - Neither Agree nor Disagree'),
    (2, '2 - Disagree'),
    (1, '1 - Strongly Disagree'),
    (0, 'na - Not Applicable'),
)

KSA_CHOICES = (
    (5, '5 - Advanced'),
    (4, '4 - Intermediate'),
    (3, '3 - Basic'),
    (2, '2 - Little'),
    (1, '1 - None'),
    (0, 'na - Not Applicable'),
)

CONFLICT = (
    ('1', 'Individual personality(s) or unwillingness to cooperate'),
    ('2', 'Environment (problems with the facility or POC, etc.)'),
    ('3', 'Individual(s) did not teach appropriate modules'),
    ('4', 'Other'),
)

YES_NO_MAYBE = (
    ('Y', 'Yes'),
    ('N', 'No'),
    ('M', 'Maybe'),
)
