from django import forms
from django.forms import Textarea, RadioSelect
from ndptc.deliveries.models import Evaluation, Delivery
from ndptc.courses.models import Question
from ndptc.utilities.templatetags.utility_tags import index_to_char_0
from .defs import EVALUATION_QUESTIONS, SURVEY_CHOICES, YES_NO_MAYBE, CONFLICT, INSTRUCTOR_EVALUATION_QUESTIONS
from .models import InstructorEvaluation, TestResult, AfterActionReport


class EvaluationForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        delivery = kwargs.pop('delivery')
        assert isinstance(delivery, Delivery), "delivery must be of type Deliveries."

        super(EvaluationForm, self).__init__(*args, **kwargs)
        self.instance.delivery = delivery

        for i in range(1, 24):
            self.fields['q%i' % i].label = EVALUATION_QUESTIONS[i - 1]
            self.fields['q%i' % i].widget.attrs = {'class': 'span4'}

        for i in range(24, 28):
            self.fields['q%i' % i] = forms.CharField(label=EVALUATION_QUESTIONS[i - 1],
                                                     widget=forms.Textarea(attrs={'class': 'span6'}),
                                                     required=False)

    class Meta:
        model = Evaluation
        exclude = ('delivery',)


class InstructorEvaluationForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        self.instructor = kwargs.pop('instructor')
        self.delivery = kwargs.pop('delivery')
        super(InstructorEvaluationForm, self).__init__(*args, **kwargs)
        self.fields['question1'].label = INSTRUCTOR_EVALUATION_QUESTIONS[0]
        self.fields['question2'].label = INSTRUCTOR_EVALUATION_QUESTIONS[1]
        self.fields['question3'].label = INSTRUCTOR_EVALUATION_QUESTIONS[2]
        self.fields['question4'].label = INSTRUCTOR_EVALUATION_QUESTIONS[3]
        self.fields['question5'].label = INSTRUCTOR_EVALUATION_QUESTIONS[4]
        self.fields['question6'].label = INSTRUCTOR_EVALUATION_QUESTIONS[5]
        self.fields['comment'].label = INSTRUCTOR_EVALUATION_QUESTIONS[6]

    def save(self, commit=True):
        self.instance.delivery = self.delivery
        self.instance.instructor = self.instructor
        return super(InstructorEvaluationForm, self).save(commit)

    class Meta:
        model = InstructorEvaluation
        exclude = ('delivery', 'instructor')


class QuestionForm(forms.Form):
    answers = forms.ChoiceField(widget=forms.RadioSelect(), label='', required=False)

    def __init__(self, question, *args, **kwargs):
        super(QuestionForm, self).__init__(*args, **kwargs)
        self.question = question
        answers = self.question.answer_set.all()
        self.fields['answers'].widget = forms.RadioSelect(attrs={'tabindex': self.question.sort_order})
        self.fields['answers'].choices = [(a.id, unicode(index_to_char_0(i)) + ') ' + unicode(a.answer)) for i, a in
                                          enumerate(answers)]

        for pos, answer in enumerate(answers):
            if answer.correct:
                self.correct = pos
            break

    def is_correct(self):
        if not self.is_valid():
            return False

        return self.cleaned_data['answers'] == str(self.correct)


def test_forms(test, participant, data=None):
    questions = Question.objects.filter(test=test).prefetch_related('answer_set')
    form_list = []
    for pos, question in enumerate(questions):
        try:
            if test.type == 'PRE':
                result = TestResult.objects.filter(participant=participant).get(question=question)
            else:
                result = TestResult.objects.filter(participant=participant, is_latest=True).get(question=question)
        except TestResult.DoesNotExist:
            result = None
        if result:
            if result.answer is not None:
                form_list.append(QuestionForm(question, data, prefix=pos, initial={'answers': result.answer.id}))
            else:
                form_list.append(QuestionForm(question, data, prefix=pos))
        else:
            form_list.append(QuestionForm(question, data, prefix=pos))
    return form_list


class AfterActionReportForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        self.delivery = kwargs.pop('delivery')
        super(AfterActionReportForm, self).__init__(*args, **kwargs)
        self.fields[
            'participants_a'].label = 'A. The course was a good fit for those who participated (i.e., the class make-up [experience, backgrounds, and rank] was optimal.'
        self.fields['participants_b'].label = 'B. There were enough participants to acceptably fill the class.'
        self.fields[
            'facilities_c'].label = 'C. The facilities proved appropriate for proper class execution (e.g., adequate breakout rooms and classroom space).'
        self.fields['facilities_d'].label = 'D. The class site is acceptable for future use.'
        self.fields[
            'materials_e'].label = 'E. The class materials and equipment were shipped to the best possible location.'
        self.fields['materials_f'].label = 'F. The correct class materials and necessary equipment were received.'
        self.fields[
            'materials_f_other'].label = 'If additional materials were needed, please describe the materials (regardless of whether you requested the material and had it shipped or you never received the material):'
        self.fields['materials_g_1'].label = 'Visual Aids'
        self.fields['materials_g_2'].label = 'Instructional Materials'
        self.fields['materials_g_3'].label = 'Monitoring Devices (if applicable)'
        self.fields['materials_g_4'].label = 'Printed Materials'
        self.fields['materials_g_other'].label = 'Please describe any equipment issues, if any:'
        self.fields[
            'materials_h'].label = 'H. After the class, there were no issues with packing/accounting for the materials, or tagging broken items (if alternative shipping arrangements occurred, indicate in the comment section).'
        self.fields['other_i'].label = 'I. The on-site coordinator cooperated well (if applicable).'
        self.fields['other_other'].label = 'Other Class Logistics Comments or Suggestions:'
        self.fields['instructor_a'].label = 'A. Overall, the instructional team worked as a cohesive unit.'
        self.fields[
            'instructor_a_conflict'].label = 'If you answer "1" or "2" to the previous question, please indicate to what you attribute this conflict:'
        self.fields['instructor_a_explain'].label = 'Please explain your answer to the previous question:'
        self.fields[
            'instructor_b'].label = 'B. The instructors taught the modules to which they were assigned (covering only the material prescribed for their respective module)'
        self.fields['instructor_b_comments'].label = 'Please explain your answer to the previous question:'
        self.fields['course_a'].label = 'A. Course content was clear and easy to communicate.'
        self.fields[
            'course_b'].label = 'B. In the course outline, the appropriate amount of time was allotted per module.'
        self.fields[
            'course_b_comments'].label = 'If there were issues with modules timing, please indicate which modules needed more time or attention and/or which modules did not need the amount of time allotted:'
        self.fields['course_c'].label = 'C. The terminal and enabling objectives were met by the class.'
        self.fields['course_d'].label = 'D. The content presented was current and relevant.'
        self.fields[
            'course_e'].label = 'E. As assessed by the instructor group, the participants are likely to apply the material learned in this course.'
        self.fields['course_f'].label = 'F. There are no immediate changes in course content needed.'
        self.fields['course_f_comments'].label = 'Other Course Content comments or suggestions:'
        self.fields[
            'admin_a'].label = 'A. Prior to and during the class, all parties including the coodinator, the POC, the on-site coordinator, and the lead instructor, communicated effectively and kept the lead well informed and prepared.'
        self.fields[
            'admin_b'].label = 'B. The class roster was sent to the lead prior to class in an acceptable amount of time.'
        self.fields[
            'admin_c'].label = 'C. Registration ran smoothly and there were no problems with the forms/questions.'
        self.fields[
            'admin_d'].label = 'D. The pre- and post-test administration ran smoothly and there were no problems with the tests or questions.'
        self.fields['admin_e'].label = 'E. The POC was helpful, readily available, and met expectations.'
        self.fields['admin_comments'].label = 'Other Class Administration comments or suggestions:'
        self.fields['general_a'].label = 'A. There was no code of conduct violation by any involved party.'
        self.fields['general_b'].label = 'B. Hotel accommodations were acceptable.'
        self.fields['general_c'].label = 'C. Ground transportation arrangements were cosiderate of all instructors.'
        self.fields['general_d'].label = 'D. Overall, the class was a success.'
        self.fields[
            'general_comments_1'].label = '1. What other difficulties, if any, did you encounter during this class?'
        self.fields[
            'general_comments_2'].label = '2. What imporovements (content, logistics, etc.) would you make to this course?'
        self.fields['general_comments_3'].label = '3. What improvements would make this class delivery more effective?'
        self.fields[
            'general_comments_4_1'].label = '4. Were the participants interested in hosting additional courses or the same course again?'
        self.fields['general_comments_4_2'].label = 'If yes which courses?:'
        self.fields['general_comments_5'].label = '5. Does the NDPTC staff need to follow-up on any issues?:'

    class Meta:
        model = AfterActionReport
        exclude = ('delivery',)

    def instructor_a_conflict(self):
        conflict = None
        try:
            team_work = self.cleaned_data['Instructor_A']
            if int(team_work) == 1 or int(team_work) == 2:
                try:
                    conflict = self.cleaned_data['Instructor_A_Conflict']
                except:
                    raise forms.ValidationError("Your score for the previous question warrants a response.")
        except:
            pass
        return conflict

    def save(self, commit=True):
        self.instance.delivery = self.delivery
        return super(AfterActionReportForm, self).save(commit)