from django.conf import settings
from django.contrib import messages
from django.shortcuts import get_object_or_404, redirect
from django.views.generic.base import TemplateView, View
from ndptc.courses.models import TestType, Test, Answer
from ndptc.deliveries.forms import test_forms, EvaluationForm
from ndptc.participants.models import Participant, TestResult, TestScore


class EvaluationBaseView(TemplateView):

    def get_context_data(self, **kwargs):
        return {
            'delivery': self.participant.delivery,
            'form': self.form,
            'participant': self.participant,
        }

    def get_form(self):
        return EvaluationForm(self.request.POST or None, delivery=self.participant.delivery)

    def dispatch(self, request, pk, *args, **kwargs):
        self.participant = get_object_or_404(Participant, pk=pk)

        error, message = self.view_check()
        if error:
            messages.error(request, message)
            return redirect(self.redirect_url())

        if self.participant.evaluation:
            messages.warning(request, 'You have already taken this evaluation')
            return redirect(self.redirect_url())
        self.form = self.get_form()
        return super(EvaluationBaseView, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        if self.form.is_valid():
            self.form.save()
            self.participant.evaluation = True
            self.participant.save()
            messages.success(request, 'Your evaluation has been saved. Thank you for the feedback')
            return redirect(self.success_url())
        return self.render_to_response(self.get_context_data())


class DeleteParticipantBaseView(View):
    def get(self, request, pk, *args, **kwargs):
        self.participant = get_object_or_404(Participant, pk=pk)
        TestScore.objects.filter(participant=self.participant).delete()
        TestResult.objects.filter(participant=self.participant).delete()
        self.participant.delete()
        messages.success(request, 'The participant record has been deleted')
        return redirect(self.redirect_url())


class TestBaseView(TemplateView):
    template_name = 'test_results.html'

    def get_context_data(self, **kwargs):
        return {
            'delivery': self.delivery,
            'participant': self.participant,
            'person': self.person,
            'test_type': self.test_type,
            'form_list': self.form_list
        }

    def dispatch(self, request, pk, type, *args, **kwargs):
        self.participant = get_object_or_404(Participant, pk=pk)
        self.person = self.participant.person
        self.delivery = self.participant.delivery
        self.test_type = get_object_or_404(TestType, type=type)

        # If the view needs to do something special, do it in the view_check method
        error, message = self.view_check()
        if error:
            messages.error(request, message)
            return redirect(self.redirect_url())

        if self.delivery.reported:
            messages.warning(request, "The delivery has already been reported and is no longer excepting data")
            return redirect(self.redirect_url())
        try:
            self.test = self.participant.delivery.tests.get(type=self.test_type)
        except Test.DoesNotExist:
            messages.error(request, "The delivery does not have a test of type {0}".format(self.test_type))
            return redirect(self.redirect_url())
        self.form_list = test_forms(self.test, self.participant, data=request.POST or None)

        return super(TestBaseView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        return self.render_to_response(self.get_context_data())

    def post(self, request, *args, **kwargs):
        forms_valid = True
        for form in self.form_list:
            if form.is_valid():
                answer = form.cleaned_data['answers']
                try:
                    answer = Answer.objects.get(pk=answer)
                except Answer.DoesNotExist:
                    answer = None
                question = form.question

                if self.test_type.pk == 'PRE':
                    TestResult.objects.get_or_create(participant=self.participant, question=question,
                                                                  defaults={'attempt': 1, 'answer': answer})
                if self.test_type.pk == 'POST':
                    #Get the old test set latest to false and create new test
                    try:
                        result = TestResult.objects.get(participant=self.participant, question=question, is_latest=True)
                        result.is_latest = False
                        result.save()
                        TestResult.objects.create(participant=self.participant, question=question, is_latest=True,
                                                  answer=answer, attempt=self.participant.post_test + 1)
                    except TestResult.DoesNotExist:
                        TestResult.objects.create(participant=self.participant, question=question, is_latest=True,
                                                  attempt=1, answer=answer)
        if forms_valid:
            self.update_test_score()
            messages.success(request, 'The test results have been saved')
            return redirect(self.success_url())

        return self.render_to_response(self.get_context_data())

    def update_test_score(self):
        results = TestResult.objects.filter(participant=self.participant, question__test__type=self.test_type,
                                            is_latest=True)
        correct = 0
        for result in results:
            if result.answer:
                if result.answer.correct:
                    correct += 1
        computed_score = int(float(correct) / float(results.count()) * 100)
        if str(self.test_type) == 'PRE':
            try:
                score = self.participant.scores.get(type=self.test_type, is_latest=True)
                score.score = computed_score
                score.save()
            except TestScore.DoesNotExist:
                score = TestScore.objects.create(type=self.test_type, score=computed_score)
                score.save()
                self.participant.scores.add(score)

        if str(self.test_type) == 'POST':
            try:
                score = self.participant.scores.get(type=self.test_type, is_latest=True)
                score.is_latest = False
                score.save()
                score = TestScore.objects.create(type=self.test_type, score=computed_score)
                score.save()
                self.participant.scores.add(score)
            except TestScore.DoesNotExist:
                score = TestScore.objects.create(type=self.test_type, score=computed_score)
                score.save()
                self.participant.scores.add(score)

        if str(self.test_type) == 'PRE':
            self.participant.pre_test = True
        else:
            self.participant.post_test += 1
            if computed_score >= settings.PASSING_SCORE:
                self.participant.attended = True
        self.participant.save()

    def view_check(self):
        return False, None


class TestResultsBaseView(TemplateView):
    template_name = 'view_test_results.html'

    def get_context_data(self, **kwargs):
        return {
            'delivery': self.participant.delivery,
            'participant': self.participant,
            'person': self.person,
            'results': self.results,
            'test_type': self.test_type,
        }

    def get(self, request, pk, type, *args, **kwargs):
        self.participant = get_object_or_404(Participant, pk=pk)
        self.person = self.participant.person
        self.test_type = get_object_or_404(TestType, type=type)

        error, message = self.view_check()
        if error:
            messages.error(request, message)
            return redirect(self.redirect_url())

        self.results = []
        test_results = TestResult.objects.filter(participant=self.participant, is_latest=True,
                                                 question__test__type=self.test_type)
        if test_results.count() < 1:
            messages.warning(request, 'There are no test results for this participant and test')
            return redirect(self.redirect_url())

        for result in test_results:
            answers = Answer.objects.filter(question=result.question)
            self.results.append({
                'question': result.question,
                'answers': answers,
                'answer': result.answer,
                'correct': result.answer.correct or None,
                'correct_answer': answers.get(correct=True),
            })

        return self.render_to_response(self.get_context_data())

    def view_check(self):
        return False, None