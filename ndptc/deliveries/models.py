from django.db import models
from django.utils import timezone
from timezone_field import TimeZoneField
from ndptc.accounts.models import DMSPerson
from ndptc.deliveries.defs import DeliveryStatus, EVALUATION_SECTIONS, EVALUATION_QUESTIONS, SURVEY_CHOICES, CONFLICT, YES_NO_MAYBE, INSTRUCTOR_EVALUATION_QUESTIONS
from ndptc.instructors.models import Availability, Taught
from ndptc.managers.managers import DeliveryManager
from ndptc.participants.models import Participant, WaitList, TestResult
from ndptc.utilities.models import Country, Language, State, Discipline, GovernmentLevel
import ndptc
import reversion

class DeliveryType(models.Model):
    """
    """
    description = models.CharField(max_length=20)
    code = models.CharField(max_length=2, unique=True)

    class Meta:
        ordering = ['description']
        db_table = 'ndptc_delivery_type'

    def __str__(self):
        return self.description


class TrainingMethod(models.Model):
    """
    """
    description = models.CharField(max_length=100)
    code = models.CharField(max_length=1, unique=True)

    class Meta:
        ordering = ['description']
        db_table = 'ndptc_training_method'

    def __str__(self):
        return self.description


class Delivery(models.Model):
    """
    """
    course = models.ForeignKey('courses.Course')
    training_method = models.ForeignKey(TrainingMethod)
    delivery_type = models.ForeignKey(DeliveryType)
    address = models.CharField(max_length=100)
    address1 = models.CharField(max_length=100, null=True, blank=True)
    city = models.CharField(max_length=30)
    state = models.ForeignKey(State)
    zip = models.CharField(max_length=25)
    country = models.ForeignKey(Country)

    # Dates
    timezone = TimeZoneField()
    start = models.DateTimeField()
    end = models.DateTimeField()
    pre_test_reminder = models.DateTimeField(blank=True)
    pre_test_close = models.DateTimeField(blank=True)
    post_test_close = models.DateTimeField(blank=True)
    evaluation_close = models.DateTimeField(blank=True)
    delivery_closed = models.DateTimeField(blank=True)
    registration_close = models.DateTimeField()


    confirmation_code = models.CharField(max_length=8, editable=False)
    reported = models.BooleanField()
    status = models.IntegerField(default=0)
    hosting_agency = models.TextField(null=True, blank=True)
    maximum_enrollment = models.IntegerField(null=True, blank=True)
    contact_information = models.TextField(null=True, blank=True)
    arrival_time = models.TextField(null=True, blank=True)
    course_materials = models.TextField(null=True, blank=True)
    internet = models.TextField(null=True, blank=True)
    food = models.TextField(null=True, blank=True)
    parking = models.TextField(null=True, blank=True)
    requirements = models.TextField(null=True, blank=True)
    additional_information = models.TextField(null=True, blank=True)
    reading_materials = models.TextField(null=True, blank=True)
    special_note = models.TextField(null=True, blank=True)
    certificates_sent = models.BooleanField(default=False)
    participant_guide = models.ForeignKey('courses.Document', null=True, blank=True)
    flyer = models.FileField(upload_to='flyers/%Y/%m', null=True, blank=True)
    language = models.ForeignKey(Language)
    is_active = models.BooleanField(default=True)
    available = models.ManyToManyField('instructors.Instructor', through=Availability,
                                       related_name='available_instructors')
    instructors = models.ManyToManyField('instructors.Instructor', through=Taught, related_name='instructors')
    tests = models.ManyToManyField('courses.Test')

    objects = DeliveryManager()

    class Meta:
        ordering = ['-start']
        db_table = 'ndptc_delivery'

    @property
    def code_required(self):
        return True if self.status & DeliveryStatus.CODE_REQUIRED else False

    @code_required.setter
    def code_required(self, value):
        assert isinstance(value, bool), 'The value must be of boolean type'
        if value:
            self.status |= DeliveryStatus.CODE_REQUIRED
        else:
            self.status &= ~DeliveryStatus.CODE_REQUIRED

    @property
    def evaluation(self):
        return True if self.status & DeliveryStatus.EVALUATION else False

    @evaluation.setter
    def evaluation(self, value):
        assert isinstance(value, bool), 'The value must be of boolean type'
        if value:
            self.status |= DeliveryStatus.EVALUATION
        else:
            self.status &= ~DeliveryStatus.EVALUATION

    @property
    def pre_test(self):
        return True if self.status & DeliveryStatus.PRE_TEST else False

    @pre_test.setter
    def pre_test(self, value):
        assert isinstance(value, bool), 'The value must be of boolean type'
        if value:
            self.status |= DeliveryStatus.PRE_TEST
        else:
            self.status &= ~DeliveryStatus.PRE_TEST

    @property
    def post_test(self):
        return True if self.status & DeliveryStatus.POST_TEST else False

    @post_test.setter
    def post_test(self, value):
        assert isinstance(value, bool), 'The value must be of boolean type'
        if value:
            self.status |= DeliveryStatus.POST_TEST
        else:
            self.status &= ~DeliveryStatus.POST_TEST

    @property
    def online(self):
        return True if self.status & DeliveryStatus.ONLINE_REGISTRATION else False

    @online.setter
    def online(self, value):
        assert isinstance(value, bool), 'The value must be of boolean type'
        if value:
            self.status |= DeliveryStatus.ONLINE_REGISTRATION
        else:
            self.status &= ~DeliveryStatus.ONLINE_REGISTRATION

    def city_state(self):
        return "%s, %s" % (self.city, self.state)

    def has_seats_available(self):
        if self.maximum_enrollment is None or self.participant_set.all().count() < self.maximum_enrollment:
            return True
        return False

    def set_active(self, active):
        self.is_active = active
        [participant.set_active(active) for participant in self.participant_set.all()]
        self.save()

    def seats_available(self):
        if self.maximum_enrollment is None:
            return '--'
        if self.maximum_enrollment <= self.participant_set.all().count():
            return 'Full'
        return self.maximum_enrollment - self.participant_set.all().count()

    def create_participant(self, user):
        if self.participant_set.filter(person=user).exists():
            return None, 'You are already registered for this course'
        participant = Participant.objects.create(person=user, delivery=self)
        participant.send_registration_email()
        return participant, 'You are now registered for a course'

    def register_for_course(self, user):
        assert type(user) is DMSPerson, 'The user must be of type DMSPerson, got type {0}'.format(type(user))
        if self.has_seats_available():
            return self.create_participant(user)
        else:
            return self.push_wait_list(user)

    def push_wait_list(self, person):
        if WaitList.objects.filter(delivery=self).filter(person=person).exists():
            return None, 'You are already on the wait list for this delivery.'
        wait_list = WaitList.objects.create(delivery=self, person=person)
        wait_list.send_wait_list_email()
        return wait_list, "The course has reached its maximum enrollment. " \
                          "You have been added to a wait list, you will be notified if you are accepted"

    def pop_wait_list(self, pk=None):
        """ Returns participant object, or raises WaitList.DoesNotExist exception.

        pop_wait_list will pop someone from the deliveries wait list. If you do not pass a pk, then the first person
        that was pushed to the wait list will be popped. If a pk is provided then the function will attempt to
        retrieve the wait list object and register the person for the delivery.

        Arguments:
        pk -- Primary key for wait list object (default None)
        """
        wait_list = WaitList.objects.get(pk=pk) if pk is not None else WaitList.objects.filter(delivery=self).first()
        if wait_list:
            participant = self.create_participant(wait_list.person.dmsperson)
            wait_list.delete()
            return participant
        raise WaitList.DoesNotExist('There are no participants in this deliveries wait list')

    @staticmethod
    def deliveries_by_course_report():
        report = []
        for course in ndptc.courses.models.Course.objects.active():
            report.append({
                'course': course,
                'deliveries': Delivery.objects.active().filter(course=course).count()
            })
        return report

    def discipline_report(self):
        values = self.participant_set.all().values('person__discipline').distinct()
        disciplines = Discipline.objects.filter(id__in=values)
        report = []
        for discipline in disciplines:
            report.append([str(discipline), self.participant_set.filter(person__discipline=discipline).count()])
        return report

    def evaluation_report(self):
        evaluations = Evaluation.objects.filter(delivery=self)
        answers = [
            [0, 0, 0, 0, 0, 0, 0, EVALUATION_QUESTIONS[i]] for i in range(0, 23)
        ]
        comments = [
            [[], EVALUATION_QUESTIONS[i + 23]] for i in range(0, 4)
        ]

        for evaluation in evaluations:
            for i in range(0, 23):
                answer = evaluation.__dict__['q%i' % (i + 1)]
                if answer is not None:
                    answers[i][answer] += 1
                    # Number of responses
                    answers[i][6] += 1

        for evaluation in evaluations:
            for i in range(0, 4):
                comment = evaluation.__dict__['q%i' % (i + 24)]
                if comment:
                    comments[i][0].append(comment)

        report = []
        for i, section in enumerate(EVALUATION_SECTIONS):
            if i == 0:
                results = answers[0:2]
            if i == 1:
                results = answers[2:6]
            if i == 2:
                results = answers[6:14]
            if i == 3:
                results = answers[14:19]
            if i == 4:
                results = answers[19:23]
            if i == 5:
                results = comments
            report.append({'section': section, 'results': results})
        return report

    def instructor_evaluation_report(self):
        report = []
        for instructor in self.instructors.all():
            answers = [
                [0, 0, 0, 0, 0, 0, 0, INSTRUCTOR_EVALUATION_QUESTIONS[i]] for i in range(0, 6)
            ]
            comments = [[], INSTRUCTOR_EVALUATION_QUESTIONS[6]]
            for evaluation in self.instructorevaluation_set.filter(instructor=instructor):
                for i in range(0, 6):
                    answer = evaluation.__dict__['question{0}'.format(i + 1)]
                    if answer is not None:
                        answers[i][answer] += 1
                        # Number of responses
                        answers[i][6] += 1
                comments[0].append(evaluation.comment)
            report.append([instructor, {'answers': answers, 'comments': comments}])
        return report

    def government_level_report(self):
        values = self.participant_set.all().values('person__government_level').distinct()
        government_levels = GovernmentLevel.objects.filter(id__in=values)
        report = []
        for government_level in government_levels:
            report.append([str(government_level), self.participant_set.filter(
                person__government_level=government_level).count()])
        return report

    def participants_report(self):
        return [
            ['Total', self.participant_set.all().count(), ],
            ['Incomplete Pre-Test', self.participant_set.filter(pre_test=False).count()],
            ['Incomplete Post-Test', self.participant_set.filter(post_test=False).count()],
            ['Attended the Course', self.participant_set.filter(attended=True).count()],
            ['Passed the Course', Participant.objects.passed().filter(delivery=self).count()],
            ['Failed the Course', Participant.objects.failed().filter(delivery=self).count()],
        ]

    def state_report(self):
        values = self.participant_set.all().values('person__state').distinct()
        states = State.objects.filter(id__in=values)
        report = []
        for state in states:
            report.append([str(state), self.participant_set.filter(person__state=state).count()])
        return report

    def test_report(self, test_type):
        try:
            questions = ndptc.courses.models.Question.objects.filter(test=self.tests.get(type=test_type))
        except ndptc.courses.models.Test.DoesNotExist:
            return None
        results = TestResult.objects.filter(participant__delivery=self)
        report = []
        for question in questions:
            response = {"question": question.question}
            question_results = results.filter(question=question)
            total_responded = question_results.count()
            answers = []
            for answer in ndptc.courses.models.Answer.objects.filter(question=question):
                num_selected = question_results.filter(answer=answer).count()
                if total_responded == 0:
                    percent = "%.1f" % 0
                else:
                    percent = "%.1f" % ((float(num_selected) / float(total_responded)) * 100.0)
                answers.append({"answer": answer.answer, "num_selected": num_selected,
                                "percentage": percent,
                                "correct": answer.correct})
            response["answers"] = answers
            report.append(response)
        return report

    def has_after_action(self):
        if AfterActionReport.objects.filter(delivery=self).exists():
            return True
        return False

    def __str__(self):
        return "%s - %s (%s)" % (self.start.strftime('%m/%d/%Y'), self.course.name, self.city_state())
reversion.register(Delivery)


class DeliveryRequest(models.Model):
    user = models.ForeignKey(DMSPerson)
    submit = models.DateTimeField(default=timezone.now())
    course = models.ForeignKey('courses.Course')
    address_1 = models.CharField(max_length=100)
    address1_1 = models.CharField(max_length=100, null=True, blank=True)
    city_1 = models.CharField(max_length=30)
    state_1 = models.ForeignKey(State, related_name='state_1')
    zip_1 = models.CharField(max_length=25)
    country_1 = models.ForeignKey(Country, related_name='country_1')
    start = models.DateTimeField()
    end = models.DateTimeField()
    participant_count = models.IntegerField()
    agency1 = models.CharField(max_length=50)
    contact_name1 = models.CharField(max_length=50)
    address_2 = models.CharField(max_length=100)
    address1_2 = models.CharField(max_length=100, null=True, blank=True)
    city_2 = models.CharField(max_length=30)
    state_2 = models.ForeignKey(State, related_name='state_2')
    zip_2 = models.CharField(max_length=25)
    country_2 = models.ForeignKey(Country, related_name='country_2')
    phone1 = models.CharField(max_length=25)
    cell1 = models.CharField(max_length=25, null=True, blank=True)
    fax1 = models.CharField(max_length=25, null=True, blank=True)
    email1 = models.CharField(max_length=200)
    agency2 = models.CharField(max_length=50, null=True, blank=True)
    contact_name2 = models.CharField(max_length=50, null=True, blank=True)
    address_3 = models.CharField(max_length=100, null=True, blank=True)
    address1_3 = models.CharField(max_length=100, null=True, blank=True)
    city_3 = models.CharField(max_length=30, null=True, blank=True)
    state_3 = models.ForeignKey(State, null=True, blank=True, related_name='state_3')
    zip_3 = models.CharField(max_length=25, null=True, blank=True)
    country_3 = models.ForeignKey(Country, null=True, blank=True, related_name='country_3')
    phone2 = models.CharField(max_length=25, null=True, blank=True)
    cell2 = models.CharField(max_length=25, null=True, blank=True)
    fax2 = models.CharField(max_length=25, null=True, blank=True)
    email2 = models.CharField(max_length=200, null=True, blank=True)
    reg_contact_name = models.CharField(max_length=100, null=True, blank=True)
    reg_phone = models.CharField(max_length=25, null=True, blank=True)
    reg_email = models.CharField(max_length=200)
    flyer = models.BooleanField()
    registration_code = models.BooleanField()
    clearance = models.BooleanField()
    clearance_details = models.CharField(max_length=200, null=True, blank=True)
    paperwork = models.BooleanField()
    paperwork_details = models.CharField(max_length=200, null=True, blank=True)
    classroom_facility = models.CharField(max_length=50)
    facility_address = models.CharField(max_length=100)
    facility_address1 = models.CharField(max_length=100, null=True, blank=True)
    facility_city = models.CharField(max_length=30)
    facility_state = models.ForeignKey(State, related_name='facility_state')
    facility_zip = models.CharField(max_length=25)
    facility_country = models.ForeignKey(Country, related_name='facility_country')
    facility_contact = models.CharField(max_length=50)
    facility_phone = models.CharField(max_length=25)
    facility_email = models.CharField(max_length=200)
    facility_shipping_address = models.CharField(max_length=100, null=True, blank=True)
    facility_shipping_address1 = models.CharField(max_length=100, null=True, blank=True)
    facility_shipping_city = models.CharField(max_length=30, null=True, blank=True)
    facility_shipping_state = models.ForeignKey(State, null=True, blank=True, related_name='facility_shipping_state')
    facility_shipping_zip = models.CharField(max_length=25, null=True, blank=True)
    facility_shipping_country = models.ForeignKey(Country, null=True, blank=True, related_name='facility_shipping_country')
    shipping_contact = models.CharField(max_length=50, null=True, blank=True)
    shipping_contact_phone = models.CharField(max_length=25, null=True, blank=True)
    shipping_contact_email = models.CharField(max_length=200, null=True, blank=True)
    instructor_access_time = models.TimeField()
    participant_access_time = models.TimeField()
    computer_lab = models.BooleanField()
    instructor_laptop = models.BooleanField()
    instructor_projection_screen = models.BooleanField()
    instructor_projector = models.BooleanField()
    instructor_internet_hard = models.BooleanField()
    instructor_internet_wireless = models.BooleanField()
    instructor_speakers = models.BooleanField()
    instructor_podium = models.BooleanField()
    av_support = models.BooleanField()
    registration_name = models.CharField(max_length=50, null=True, blank=True)
    registration_phone = models.CharField(max_length=25, null=True, blank=True)
    av_name = models.CharField(max_length=50, null=True, blank=True)
    av_phone = models.CharField(max_length=25, null=True, blank=True)
    parking = models.BooleanField()
    parking_requirements = models.CharField(max_length=200, null=True, blank=True)
    secure = models.BooleanField()
    food = models.BooleanField()
    food_details = models.CharField(max_length=200, null=True, blank=True)
    beverage = models.BooleanField()
    vending = models.BooleanField()
    airport = models.CharField(max_length=100, null=True, blank=True)
    airport_distance = models.FloatField()
    hotel = models.CharField(max_length=100, null=True, blank=True)
    hotel_distance = models.FloatField()
    delivery = models.ForeignKey(Delivery, null=True, blank=True)

    class Meta:
        db_table = 'ndptc_delivery_request'

    def __str__(self):
        return '{0} - {1}: {2}'.format(self.start.strftime("%m/%d/%Y"), str(self.course), self.user.get_full_name())

    def initial_data(self):
        data = {
            'course': self.course,
            'start': self.start,
            'end': self.end,
            'parking': self.parking_requirements,
            'food': self.food_details,
            'arrival_time': self.participant_access_time,
            'contact_information': self.contact_name1,
            'hosting_agency': self.agency1,
            'maximum_enrollment': self.participant_count,
            'code_required': self.registration_code,
            'online': self.flyer,
            'address': self.address_1,
            'address1': self.address1_1,
            'city': self.city_1,
            'state': self.state_1,
            'zip': self.zip_1,
            'country': self.country_1
        }
        return data
reversion.register(DeliveryRequest)

class Evaluation(models.Model):
    delivery = models.ForeignKey(Delivery)
    q1 = models.IntegerField(null=True, blank=True, choices=SURVEY_CHOICES)
    q2 = models.IntegerField(null=True, blank=True, choices=SURVEY_CHOICES)
    q3 = models.IntegerField(null=True, blank=True, choices=SURVEY_CHOICES)
    q4 = models.IntegerField(null=True, blank=True, choices=SURVEY_CHOICES)
    q5 = models.IntegerField(null=True, blank=True, choices=SURVEY_CHOICES)
    q6 = models.IntegerField(null=True, blank=True, choices=SURVEY_CHOICES)
    q7 = models.IntegerField(null=True, blank=True, choices=SURVEY_CHOICES)
    q8 = models.IntegerField(null=True, blank=True, choices=SURVEY_CHOICES)
    q9 = models.IntegerField(null=True, blank=True, choices=SURVEY_CHOICES)
    q10 = models.IntegerField(null=True, blank=True, choices=SURVEY_CHOICES)
    q11 = models.IntegerField(null=True, blank=True, choices=SURVEY_CHOICES)
    q12 = models.IntegerField(null=True, blank=True, choices=SURVEY_CHOICES)
    q13 = models.IntegerField(null=True, blank=True, choices=SURVEY_CHOICES)
    q14 = models.IntegerField(null=True, blank=True, choices=SURVEY_CHOICES)
    q15 = models.IntegerField(null=True, blank=True, choices=SURVEY_CHOICES)
    q16 = models.IntegerField(null=True, blank=True, choices=SURVEY_CHOICES)
    q17 = models.IntegerField(null=True, blank=True, choices=SURVEY_CHOICES)
    q18 = models.IntegerField(null=True, blank=True, choices=SURVEY_CHOICES)
    q19 = models.IntegerField(null=True, blank=True, choices=SURVEY_CHOICES)
    q20 = models.IntegerField(null=True, blank=True, choices=SURVEY_CHOICES)
    q21 = models.IntegerField(null=True, blank=True, choices=SURVEY_CHOICES)
    q22 = models.IntegerField(null=True, blank=True, choices=SURVEY_CHOICES)
    q23 = models.IntegerField(null=True, blank=True, choices=SURVEY_CHOICES)
    q24 = models.TextField(null=True, blank=True)
    q25 = models.TextField(null=True, blank=True)
    q26 = models.TextField(null=True, blank=True)
    q27 = models.TextField(null=True, blank=True)

    class Meta:
        db_table = 'ndptc_evaluation'
reversion.register(Evaluation)


class InstructorEvaluation(models.Model):
    """
    """
    instructor = models.ForeignKey('instructors.Instructor')
    delivery = models.ForeignKey('Delivery')
    question1 = models.IntegerField(null=True, blank=True, choices=SURVEY_CHOICES)
    question2 = models.IntegerField(null=True, blank=True, choices=SURVEY_CHOICES)
    question3 = models.IntegerField(null=True, blank=True, choices=SURVEY_CHOICES)
    question4 = models.IntegerField(null=True, blank=True, choices=SURVEY_CHOICES)
    question5 = models.IntegerField(null=True, blank=True, choices=SURVEY_CHOICES)
    question6 = models.IntegerField(null=True, blank=True, choices=SURVEY_CHOICES)
    comment = models.TextField(null=True, blank=True,)

    class Meta:
        db_table = 'ndptc_instructor_evaluation'
reversion.register(InstructorEvaluation)


class AfterActionReport(models.Model):
    delivery = models.ForeignKey('deliveries.Delivery', unique=True)
    participants_a = models.IntegerField(null=True, blank=True, choices=SURVEY_CHOICES)
    participants_b = models.IntegerField(null=True, blank=True, choices=SURVEY_CHOICES)
    facilities_c = models.IntegerField(null=True, blank=True, choices=SURVEY_CHOICES)
    facilities_d = models.IntegerField(null=True, blank=True, choices=SURVEY_CHOICES)
    materials_e = models.IntegerField(null=True, blank=True, choices=SURVEY_CHOICES)
    materials_f = models.IntegerField(null=True, blank=True, choices=SURVEY_CHOICES)
    materials_f_other = models.TextField(null=True, blank=True)
    materials_g_1 = models.IntegerField(null=True, blank=True, choices=SURVEY_CHOICES)
    materials_g_2 = models.IntegerField(null=True, blank=True, choices=SURVEY_CHOICES)
    materials_g_3 = models.IntegerField(null=True, blank=True, choices=SURVEY_CHOICES)
    materials_g_4 = models.IntegerField(null=True, blank=True, choices=SURVEY_CHOICES)
    materials_g_other = models.TextField(null=True, blank=True)
    materials_h = models.IntegerField(null=True, blank=True, choices=SURVEY_CHOICES)
    other_i = models.IntegerField(null=True, blank=True, choices=SURVEY_CHOICES)
    other_other = models.TextField(null=True, blank=True)
    instructor_a = models.IntegerField(null=True, blank=True, choices=SURVEY_CHOICES)
    instructor_a_conflict = models.IntegerField(null=True, blank=True, choices=CONFLICT)
    instructor_a_explain = models.TextField(null=True, blank=True)
    instructor_b = models.IntegerField(null=True, blank=True, choices=SURVEY_CHOICES)
    instructor_b_comments = models.TextField(null=True, blank=True)
    course_a = models.IntegerField(null=True, blank=True, choices=SURVEY_CHOICES)
    course_b = models.IntegerField(null=True, blank=True, choices=SURVEY_CHOICES)
    course_b_comments = models.TextField(null=True, blank=True)
    course_c = models.IntegerField(null=True, blank=True, choices=SURVEY_CHOICES)
    course_d = models.IntegerField(null=True, blank=True, choices=SURVEY_CHOICES)
    course_e = models.IntegerField(null=True, blank=True, choices=SURVEY_CHOICES)
    course_f = models.IntegerField(null=True, blank=True, choices=SURVEY_CHOICES)
    course_f_comments = models.TextField(null=True, blank=True)
    admin_a = models.IntegerField(null=True, blank=True, choices=SURVEY_CHOICES)
    admin_b = models.IntegerField(null=True, blank=True, choices=SURVEY_CHOICES)
    admin_c = models.IntegerField(null=True, blank=True, choices=SURVEY_CHOICES)
    admin_d = models.IntegerField(null=True, blank=True, choices=SURVEY_CHOICES)
    admin_e = models.IntegerField(null=True, blank=True, choices=SURVEY_CHOICES)
    admin_comments = models.TextField(null=True, blank=True)
    general_a = models.IntegerField(null=True, blank=True, choices=SURVEY_CHOICES)
    general_b = models.IntegerField(null=True, blank=True, choices=SURVEY_CHOICES)
    general_c = models.IntegerField(null=True, blank=True, choices=SURVEY_CHOICES)
    general_d = models.IntegerField(null=True, blank=True, choices=SURVEY_CHOICES)
    general_comments_1 = models.TextField(null=True, blank=True)
    general_comments_2 = models.TextField(null=True, blank=True)
    general_comments_3 = models.TextField(null=True, blank=True)
    general_comments_4_1 = models.CharField(max_length=1, null=True, blank=True,choices=YES_NO_MAYBE)
    general_comments_4_2 = models.TextField(null=True, blank=True)
    general_comments_5 = models.TextField(null=True, blank=True)

    class Meta:
        db_table = 'ndptc_after_action'
reversion.register(AfterActionReport)
