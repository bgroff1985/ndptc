from django import forms
from ndptc.accounts.models import NDPTCPerson, DMSPerson
from uuid import uuid4


class NDPTCPeopleForm(forms.ModelForm):
    class Meta:
        exclude = ['is_generated']
        model = NDPTCPerson


class DMSPersonForm(forms.ModelForm):
    class Meta:
        exclude = ['is_generated', 'username', 'password', 'last_login', 'date_joined']
        model = DMSPerson

        widgets = {
            'email': forms.TextInput(attrs={'class': 'span6'}),
            'last_name': forms.TextInput(attrs={'class': 'span6'}),
            'first_name': forms.TextInput(attrs={'class': 'span6'}),
            'middle': forms.TextInput(attrs={'class': 'span1'}),
            'fema_id': forms.TextInput(attrs={'class': 'span2'}),
            'title': forms.TextInput(attrs={'class': 'span6'}),
            'agency': forms.TextInput(attrs={'class': 'span6'}),
            'government_level': forms.Select(attrs={'class': 'span6'}),
            'discipline': forms.Select(attrs={'class': 'span6'}),
            'phone': forms.TextInput(attrs={'class': 'span3'}),
            'address': forms.TextInput(attrs={'class': 'span6'}),
            'address1': forms.TextInput(attrs={'class': 'span6'}),
            'city': forms.TextInput(attrs={'class': 'span3'}),
            'state': forms.Select(attrs={'class': 'span6'}),
            'zip': forms.TextInput(attrs={'class': 'span3'}),
            'country': forms.Select(attrs={'class': 'span6'}),
        }

    def save(self, commit=True):
        self.instance.username = str(uuid4())
        self.instance.set_unusable_password()
        self.instance.is_active = False
        self.instance.is_generated = True
        return super(DMSPersonForm, self).save(commit)


class DMSPersonWithUserForm(forms.ModelForm):
    class Meta:
        exclude = ['is_generated', 'password', 'last_login', 'date_joined']
        model = DMSPerson

        widgets = {
            'username': forms.TextInput(attrs={'class': 'span6'}),
            'email': forms.TextInput(attrs={'class': 'span6'}),
            'last_name': forms.TextInput(attrs={'class': 'span6'}),
            'first_name': forms.TextInput(attrs={'class': 'span6'}),
            'middle': forms.TextInput(attrs={'class': 'span1'}),
            'fema_id': forms.TextInput(attrs={'class': 'span2'}),
            'title': forms.TextInput(attrs={'class': 'span6'}),
            'agency': forms.TextInput(attrs={'class': 'span6'}),
            'government_level': forms.Select(attrs={'class': 'span6'}),
            'discipline': forms.Select(attrs={'class': 'span6'}),
            'phone': forms.TextInput(attrs={'class': 'span3'}),
            'address': forms.TextInput(attrs={'class': 'span6'}),
            'address1': forms.TextInput(attrs={'class': 'span6'}),
            'city': forms.TextInput(attrs={'class': 'span3'}),
            'state': forms.Select(attrs={'class': 'span6'}),
            'zip': forms.TextInput(attrs={'class': 'span3'}),
            'country': forms.Select(attrs={'class': 'span6'}),
        }

    def save(self, commit=True):
        self.instance.set_unusable_password()
        return super(DMSPersonWithUserForm, self).save(commit)
