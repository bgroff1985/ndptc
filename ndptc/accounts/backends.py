from django.contrib.auth import get_user_model
import requests
import re

User = get_user_model()


class UHBackend:
    """
    The UHBackend is an authentication backend used to authenticate University of Hawaii users in the
    DMS. The backend uses the UH CAS system (http://www.hawaii.edu/infotech/middleware/weblogin/) to check
    if the user is authenticated. Lastly if the UH user does authenticate, but does not exist within our system,
    the user will then be created and an unusable password will be set.
    """
    def authenticate(self, username=None, password=None):
        try:
            user = User.objects.get(username=username)
        except User.DoesNotExist:
            return None

        # Check that the user is a uh_user
        if not user.is_uh:
            return None

        # Post the username and password to the UH CAS system. In the return response a ticket number will be some HTML
        request = requests.post(
            'https://login.its.hawaii.edu/cas/login?service=https://ndptc.hawaii.edu',
            data={'username': username, 'password': password})

        # Use a regular expression to find the ticket number. The UH CAS system returns HTML with the ticket
        # buried in a tag. The regex will look for start-> 'ticket=' ticket_number '";' <- at the end
        ticket = re.search('ticket=(.*)";', request.text)

        # If we do not have a ticket then we failed to authenticate, OR UH CHANGED THE WAY THEY DO BUSINESS
        if ticket is None:
            return None
        ticket = ticket.group(1)

        # Then we validate the ticket number with the UH CAS system, which will either return 'no' (Not valid)
        # or will return 'yes' with the username and some other information that is not particularly useful to us.
        result = requests.get(
            'https://login.its.hawaii.edu/cas/validate?service=https://ndptc.hawaii.edu&ticket=%s' % ticket)
        result = result.text.split('\n')
        if result[0] == 'yes':
            username = result[1]
            # Get the user from the UH Username, also set the username to all lowercase as Django us picky about
            # case, but UH is not.
            try:
                user = User.objects.get(username=username.lower())
            # If the user does not exist in our system, create a User object.
            except User.DoesNotExist:
                user = User()
                user.username = username
                user.email = username + "@hawaii.edu"
                user.set_unusable_password()
                user.save()
            return user
        return None

    def get_user(self, user_pk):
        try:
            return User.objects.get(pk=user_pk)
        except User.DoesNotExist:
            return None
