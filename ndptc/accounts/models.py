import re
from django.utils import timezone
from django.core import validators
from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, UserManager
from django.core.mail import send_mail
from django.utils.encoding import python_2_unicode_compatible
from django.utils.http import urlquote
from django.utils.translation import ugettext_lazy as _
from ndptc.instructors.models import Instructor
from ndptc.utilities.models import State, Country, GovernmentLevel, Discipline
import reversion


@python_2_unicode_compatible
class NDPTCPerson(AbstractBaseUser, PermissionsMixin):
    """
    """
    username = models.CharField(_('username'), max_length=255, unique=True,
                                help_text=_('Required. 255 characters or fewer. Letters, numbers and '
                                            '@/./+/-/_ characters'),
                                validators=[
                                    validators.RegexValidator(re.compile('^[\w.@+-]+$'), _('Enter a valid username.'),
                                                              'invalid')
                                ])
    first_name = models.CharField(_('first name'), max_length=50)
    last_name = models.CharField(_('last name'), max_length=50)
    email = models.EmailField(_('email address'))#, unique=True)
    is_active = models.BooleanField(_('active'), default=True,
                                    help_text=_('Designates whether this user should be treated as '
                                                'active. Unselect this instead of deleting accounts.'))
    is_uh = models.BooleanField(_('uh user'), default=False,
                                help_text=_('Designates whether this user should be treated as '
                                            'a University of Hawaii user.'))
    is_generated = models.BooleanField(default=False)
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)

    objects = UserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')
        db_table = 'ndptc_person'

    def get_absolute_url(self):
        return "/accounts/%s/" % urlquote(self.pk)

    def get_full_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        """
        Returns the short name for the user.
        """
        return self.first_name

    def get_username(self):
        if self.is_generated:
            return None
        return self.username

    def email_user(self, subject, message, from_email=None):
        """
        Sends an email to this User.
        """
        send_mail(subject, message, from_email, [self.email])

    def __str__(self):
        return ''#"{0} {1}".format(self.first_name, self.last_name)
reversion.register(NDPTCPerson)


@python_2_unicode_compatible
class DMSPerson(NDPTCPerson):
    #: The persons middle initial. See RES Page 7. Table 4, Row: 4
    middle = models.CharField(max_length=1, null=True, blank=True)
    #: Title within the an organization. See RES Page 7. Table 4, Row: 6
    title = models.CharField(max_length=100, null=True, blank=True)
    #: The person's agency's name. See RES Page 7. Table 4, Row: 5
    agency = models.CharField(max_length=100, null=True, blank=True)
    #: Foreign key to an address. See RES Page 7. Table 4, Row: 7-13.
    #: Also see the Address class in the Utilties module.
    address = models.CharField(max_length=100)
    address1 = models.CharField(max_length=100, null=True, blank=True)
    city = models.CharField(max_length=30)
    state = models.ForeignKey(State)
    zip = models.CharField(max_length=25)
    country = models.ForeignKey(Country)
    #: The person's work phone. See RES Page 7. Table 4, Row: 14
    phone = models.CharField(max_length=25)
    #: The person's level in government. See RES Page 7. Table 4, Row: 16.
    #: Also see Discipline class in FEMA module.
    government_level = models.ForeignKey(GovernmentLevel)
    #: The students discipline. See RES Page 7. Table 4, Row: 17.
    #: Also see GovernemntLevel class in FEMA module.
    discipline = models.ForeignKey(Discipline)
    #: An ID that is created from CDP
    fema_id = models.CharField(max_length=10, null=True, blank=True)
    #: Added field used to filter citizens vs. non-citizens.
    citizen = models.BooleanField()

    objects = UserManager()

    class Meta:
        db_table = 'ndptc_dms_person'
        ordering = ('last_name', 'first_name')

    def email_help(self, request):
        pass

    def get_address(self):
        return '{0} {1}\n{2}, {3}. {4}'.format(self.address, self.address1, self.city, self.state, self.zip)

    def is_instructor(self):
        try:
            Instructor.objects.active().get(person=self)
            return True
        except Instructor.DoesNotExist:
            return False

    def __str__(self):
        return self.get_full_name()
reversion.register(DMSPerson)
