from django.db import models
from ndptc.managers.managers import ActiveManager


class Country(models.Model):
    iso_code = models.CharField(max_length=3, unique=True)
    name = models.CharField(max_length=50)
    fema_code = models.CharField(max_length=2)
    sort_order = models.IntegerField()
    is_active = models.BooleanField(default=True)

    objects = ActiveManager()

    class Meta:
        ordering = ['sort_order']
        db_table = 'ndptc_country'

    def __str__(self):
        return self.name


class Language(models.Model):
    code = models.CharField(max_length=4, unique=True)
    language = models.CharField(max_length=50)
    is_active = models.BooleanField(default=True)

    objects = ActiveManager()

    class Meta:
        ordering = ['code']
        db_table = 'ndptc_languages'

    def __str__(self):
        return self.language


class State(models.Model):
    name = models.CharField(max_length=50)
    fema_code = models.CharField(max_length=2, unique=True)
    fema_region = models.IntegerField()
    is_active = models.BooleanField(default=True)

    objects = ActiveManager()

    class Meta:
        ordering = ['name']
        db_table = 'ndptc_state'

    def __str__(self):
        return self.name


class Discipline(models.Model):
    description = models.CharField(max_length=100)
    code = models.CharField(max_length=5)

    class Meta:
        ordering = ('description',)
        db_table = 'ndptc_discipline'

    def __str__(self):
        return self.description


class GovernmentLevel(models.Model):
    description = models.CharField(max_length=100)
    code = models.CharField(max_length=2)
    sort_order = models.IntegerField()

    class Meta:
        ordering = ('sort_order',)
        db_table = 'ndptc_government_level'

    def __str__(self):
        return self.description
