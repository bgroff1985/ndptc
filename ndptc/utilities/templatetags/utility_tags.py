from django import template

register = template.Library()

@register.filter
def index_to_char(index):
    assert isinstance(index, int), "index must be of type int"

    if index <= 26:
        return chr(index + 64)  # 64 + 1 == A, our index should start at 1
    else:
        return index_to_char(index / 26) + index_to_char_0(index % 27)

@register.filter
def index_to_char_0(index):
    assert isinstance(index, int), "index must be of type int"
    return index_to_char(index + 1)  # 65 + 0 == A, our index should start at 0
