import ndptc
import reversion
from django.db import models
from django.utils.datetime_safe import datetime
from uuidfield import UUIDField
from ndptc.accounts.models import DMSPerson
from ndptc.managers.managers import ActiveManager, ParticipantManager


class Participant(models.Model):
    """
    """
    delivery = models.ForeignKey('deliveries.Delivery')
    person = models.ForeignKey(DMSPerson)
    pre_test = models.BooleanField(default=False)
    post_test = models.IntegerField(null=False)
    attended = models.BooleanField(default=False)
    evaluation = models.BooleanField(default=False)
    passed = models.BooleanField(default=False)
    registered = models.DateTimeField(default=datetime.utcnow)
    scores = models.ManyToManyField('TestScore')
    is_active = models.BooleanField(default=True)

    objects = ParticipantManager()

    class Meta:
        db_table = 'ndptc_participant'
        unique_together = ('delivery', 'person')

    def cancel_registration(self):
        if self.attended:
            return False, 'Your registration can not be canceled because you have already completed the course'
        self.delivery.pop_wait_list()
        return True, 'Your registration has been canceled'

    @property
    def failed(self):
        return True if self.attended and not self.passed else False

    def get_status(self):
        if self.passed:
            return "Complete"
        if self.pre_test or self.post_test:
            return "In Progress"
        return "Registered"

    def send_registration_email(self):
        pass

    def set_active(self, active):
        self.is_active = active
        [score.set_active(active) for score in self.scores.all()]
        [result.set_active(active) for result in self.testresult_set.all()]
        self.save()

    @staticmethod
    def total_report():
        return {
            'total': DMSPerson.objects.count(),
            'registered': Participant.objects.active().count(),
            'attended': Participant.objects.active().attended().count(),
            'passed': Participant.objects.active().passed().count(),
            'failed': Participant.objects.active().failed().count(),
        }

    @staticmethod
    def yearly_report():
        report = []
        for year in ndptc.deliveries.models.Delivery.objects.active().dates('start', 'year'):
            year = year.year
            report.append({
                'year': year,
                'registered': Participant.objects.active().year(year).count(),
                'attended': Participant.objects.active().attended().year(year).count(),
                'passed': Participant.objects.active().passed().year(year).count(),
                'failed': Participant.objects.active().failed().year(year).count(),
            })
        return report

    def __str__(self):
        return str(self.person)

    def delete(self, force=False):
        if force is False and self.delivery.reported:
            raise Exception(
                'The participant record cannot be deleted because the course delivery has already been reported.')
        super(Participant, self).delete()

    def save(self, force=False, *args, **kwargs):
        if force is False and self.delivery.reported:
            raise Exception(
                'The participant record cannot be saved because the course delivery has already been reported.')
        if self.post_test is None:
            self.post_test = 0
        super(Participant, self).save(*args, **kwargs)
reversion.register(Participant)


class TestResult(models.Model):
    participant = models.ForeignKey('Participant')
    question = models.ForeignKey('courses.Question')
    answer = models.ForeignKey('courses.Answer', null=True)
    attempt = models.IntegerField(default=1)
    is_latest = models.BooleanField(default=True)
    is_active = models.BooleanField(default=True)

    objects = ActiveManager()

    class Meta:
        ordering = ['question__sort_order']
        db_table = 'ndptc_test_result'

    def delete(self):
        if self.participant.delivery.reported:
            raise Exception(
                'The test result record cannot be deleted because the course delivery has already been reported.')
        super(TestResult, self).delete()

    def set_active(self, active):
        self.is_active = active
        self.save()

    def save(self, force=False, *args, **kwargs):
        if force is False and self.participant.delivery.reported:
            raise Exception(
                'The test result record cannot be saved because the course delivery has already been reported.')
        super(TestResult, self).save(*args, **kwargs)
reversion.register(TestResult)


class TestScore(models.Model):
    """
    """
    #    participant = models.ForeignKey('Participants')
    type = models.ForeignKey('courses.TestType')
    score = models.IntegerField()
    attempt = models.IntegerField(default=1)
    is_latest = models.BooleanField(default=True)
    is_active = models.BooleanField(default=True)

    objects = ActiveManager()

    class Meta:
        db_table = 'ndptc_test_score'

    def delete(self):
        super(TestScore, self).delete()

    def set_active(self, active):
        self.is_active = active
        self.save()

    def save(self, *args, **kwargs):
        super(TestScore, self).save(*args, **kwargs)
reversion.register(TestScore)


class WaitList(models.Model):
    """
    """
    delivery = models.ForeignKey('deliveries.Delivery')
    person = models.ForeignKey(DMSPerson)
    insert = models.DateTimeField(default=datetime.utcnow)

    class Meta:
        ordering = ['insert']
        db_table = 'ndptc_wait_list'

    def send_wait_list_email(self):
        pass
reversion.register(WaitList)


class LevelThreeSurvey(models.Model):

    participant = models.ForeignKey(Participant)
    uuid = UUIDField(auto=True, primary_key=True)
    taken = models.BooleanField()
    question1 = models.TextField(null=True)
    question2 = models.TextField(null=True)
    question3 = models.CharField(max_length=15)
    question4 = models.TextField(null=True)
    question5 = models.TextField(null=True)
    question6 = models.TextField(null=True)
    question7 = models.TextField(null=True)
    question8 = models.TextField(null=True)
    question9 = models.TextField(null=True)
    question10 = models.CharField(max_length=2)
    question11 = models.TextField(null=True)
    question12 = models.TextField(null=True)
    question13 = models.TextField()
    question14 = models.TextField(null=True)
    question15 = models.TextField(null=True)
    question16 = models.TextField(null=True)
    question17 = models.TextField(blank=True, null=True)
    question18 = models.TextField(blank=True, null=True)
    question19 = models.TextField(blank=True, null=True)

    class Meta:
        db_table = 'ndptc_levelthreesurvey'
reversion.register(LevelThreeSurvey)


class LevelThreeSurveySend(models.Model):
    send_date = models.DateField()
    sender = models.ForeignKey(DMSPerson)

    class Meta:
        db_table = 'ndptc_levelthreesurveysend'
