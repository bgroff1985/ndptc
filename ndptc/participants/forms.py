from django import forms
from ndptc.participants.models import LevelThreeSurvey


class LevelThreeSurveyForm(forms.ModelForm):
    question1 = forms.CharField(widget=forms.Textarea(), label='1. To what extent did the course enhance your knowledge and/or skill?', required=False)
    question2 = forms.CharField(widget=forms.Textarea(), label='2. Please provide specific examples of your enhanced knowledge and/or proficiency.', required=False)
    question3 = forms.ChoiceField(widget=forms.RadioSelect, choices=(('On the job', 'On the job'), ('At home', 'At home'), ('Not at all', 'Not at all'), ('Other', 'Other')), label='3. Have you been able to apply the knowledge and/or skills gained from this course? Please indicate:')
    question4 = forms.CharField(widget=forms.Textarea(), label='4. If answered Other above, please explain:', required=False)
    question5 = forms.CharField(widget=forms.Textarea(), label='5. Have you received the necessary support from your organization to apply your new knowledge and/or skill at work?', required=False)
    question6 = forms.CharField(widget=forms.Textarea(), label='6. Which aspects of the course have proved to be of most value to you in your work? Please provide specific examples of improved results:', required=False)
    question7 = forms.CharField(widget=forms.Textarea(), label='7. Do you feel the course objectives were met?', required=False)
    question8 = forms.CharField(widget=forms.Textarea(), label='8. Are there any aspects of the course that you feel should have been handled differently?', required=False)
    question9 = forms.CharField(widget=forms.Textarea(), label='9. After taking this course and trying to implement your new knowledge/skills/abilities, are there any aspects you would like to see added to this course? Please provide examples.', required=False)
    question10 = forms.ChoiceField(widget=forms.RadioSelect, choices=(('1', '1 - Lowest'), ('2', '2'), ('3', '3'), ('4', '4'), ('5', '5 - Average'), ('6', '6'), ('7', '7'),('8', '8'), ('9', '9'), ('10', '10 - Highest')), label='10. From your perspective, please rate the overall value of the course to you on a scale of 1-10 (With 10 being the most valuable):')
    question11 = forms.CharField(widget=forms.Textarea(), label='11. Do you think refresher training will be necessary?', required=False)
    question12 = forms.CharField(widget=forms.Textarea(), label='12. What should the refresher training include?', required=False)
    question13 = forms.ChoiceField(widget=forms.RadioSelect, choices=(('3 months', '3 months'), ('3 months', '6 months'), ('One year', 'One year'), ('Two years', 'Two years'), ('Other', 'Other')), label='13. If yes, about how long after attendance on the course? Please indicate:')
    question14 = forms.CharField(widget=forms.Textarea(), label='14. If selected Other in question 13, please explain:', required=False)
    question15 = forms.CharField(widget=forms.Textarea(), label='15. Do you have any other general comments about this course?', required=False)
    question16 = forms.CharField(widget=forms.Textarea(), label='16. Would you be interested in taking an advanced course on this subject?', required=False)
    question17 = forms.CharField(widget=forms.Textarea(), label='17. How, if at all, has the course changed your understanding of the field of disaster management?', required=False)
    question18 = forms.CharField(widget=forms.Textarea(), label='18. How, if at all, did the course change your perception of the relative importance you assign to disaster management?', required=False)
    question19 = forms.CharField(widget=forms.Textarea(), label='19. How, if at all, did it change the way you think about your work with your staff, other agencies, or the community?', required=False)


    class Meta:
       model = LevelThreeSurvey
       exclude = ('participant', 'uuid', 'taken')

    def save(self):
        self.instance.taken = True
        super(LevelThreeSurveyForm, self).save()