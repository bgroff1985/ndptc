from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django.core.urlresolvers import reverse
from django.db import models
from django.http import HttpResponse
from django.utils import timezone
from django.utils.encoding import python_2_unicode_compatible
from ndptc.accounts.models import DMSPerson
from ndptc.deliveries.models import Delivery, Participant, TestResult
from ndptc.instructors.models import Certification
from ndptc.managers.managers import ActiveManager
from ndptc.utilities.models import State
from .defs import CourseStatus
import mimetypes
import reversion

@python_2_unicode_compatible
class Answer(models.Model):
    """
    """
    question = models.ForeignKey('Question')
    answer = models.TextField()
    correct = models.BooleanField(default=False)
    sort_order = models.IntegerField()

    class Meta:
        ordering = ['sort_order']
        db_table = 'ndptc_answer'

    def __str__(self):
        return self.answer
reversion.register(Answer)

@python_2_unicode_compatible
class Course(models.Model):
    """
    """
    certified = models.BooleanField()
    contact_hours = models.CharField(max_length=5)
    name = models.CharField(max_length=80)
    number = models.CharField(max_length=50, unique=True)
    course_type = models.ForeignKey('CourseType')
    description = models.TextField(null=True, blank=True)
    icon2d = models.FileField(upload_to='icons/', null=True, blank=True)
    icon3d = models.FileField(upload_to='icons/', null=True, blank=True)
    prerequisites = models.TextField(null=True, blank=True)
    requirements = models.TextField(null=True, blank=True)
    short_name = models.CharField(max_length=50)
    status = models.IntegerField()
    target_audience = models.TextField(null=True, blank=True)
    training_provider = models.ForeignKey('TrainingProvider')
    training_type = models.ForeignKey('TrainingType')

    instructors = models.ManyToManyField('instructors.Instructor', through=Certification,
                                         related_name='certified_instructors')

    is_active = models.BooleanField(default=True, blank=True)

    objects = ActiveManager()

    class Meta:
        ordering = ['name']
        db_table = 'ndptc_course'

    def set_active(self, active):
        self.is_active = active
        [delivery.set_active(active) for delivery in self.delivery_set.all()]
        self.save()

    def show_in_catalog(self):
        if self.status & CourseStatus.SHOW_IN_CATALOG:
            return True
        return False

    def get_number(self):
        if self.certified:
            return self.number
        return '----'

    def get_icon2d(self):
        if self.icon2d:
            return self.icon2d.url
        return '/media/icons/icon_default.png'

    def get_icon3d(self):
        if self.icon3d:
            return self.icon3d.url
        return '/media/icons/icon_default.png'

    def yearly_report(self):
        report = []
        for year in Delivery.objects.active().dates('start', 'year').filter(course=self):
            year = year.year
            participants = Participant.objects.active().filter(delivery__course=self).year(year)
            year_report = [0, 0, 0, 0]
            for participant in participants:
                year_report[0] += 1
                year_report[1] += 1 if participant.attended else 0
                year_report[2] += 1 if participant.passed else 0
                year_report[3] += 1 if participant.failed else 0

            report.append({
                'year': year,
                'registered': year_report[0],
                'attended': year_report[1],
                'passed': year_report[2],
                'failed': year_report[3],
            })
        return report

    def participants_by_state_report(self):
        participants = Participant.objects.active().filter(delivery__course=self)
        states = participants.distinct('person__state').values_list('person__state', flat=True)
        report = []
        for state in states:
            filtered_participants = participants.filter(person__state_id=state)
            state_report = [0, 0, 0, 0]
            for participant in filtered_participants:
                state_report[0] += 1
                state_report[1] += 1 if participant.attended else 0
                state_report[2] += 1 if participant.passed else 0
                state_report[3] += 1 if participant.failed else 0

            report.append({
                'state': str(State.objects.get(pk=state)),
                'registered': state_report[0],
                'attended': state_report[1],
                'passed': state_report[2],
                'failed': state_report[3],
            })
        return sorted(report, key=lambda k: k['state'])

    def instructors_by_state_report(self):
        certifications = Certification.objects.active().filter(course=self)
        instructors = DMSPerson.objects.filter(id__in=certifications.values_list('instructor__person', flat=True))
        states = instructors.order_by('state__id').distinct('state').values_list('state', flat=True)
        report = []
        for state in states:
            filtered_instructors = instructors.filter(state_id=state)
            report.append({
                'state': str(State.objects.get(pk=state)),
                'count': filtered_instructors.count()
            })
        return sorted(report, key=lambda k: k['state'])

    def __str__(self):
        ret_str = self.name
        if self.certified:
            ret_str += ' (' + self.number + ')'
        return ret_str
reversion.register(Course)


@python_2_unicode_compatible
class CourseType(models.Model):
    """
    """
    code = models.CharField(max_length=2, unique=True)
    description = models.CharField(max_length=100)

    class Meta:
        ordering = ['description']
        db_table = 'ndptc_course_type'

    def __str__(self):
        return self.description


class DocumentStorage(FileSystemStorage):
    """
    """

    def url(self, name):
        document = Document.objects.get(file=name)
        return unicode(reverse('document', args=[document.pk]))


document_file_storage = DocumentStorage(location=settings.DOCUMENT_ROOT)


@python_2_unicode_compatible
class DocumentType(models.Model):
    """
    """
    code = models.CharField(max_length=3, unique=True)
    name = models.CharField(max_length=50)

    class Meta:
        ordering = ['name']
        db_table = 'ndptc_document_type'

    def __str__(self):
        return self.name


def upload_document_to(instance, filename):
    return settings.DOCUMENT_ROOT + filename


@python_2_unicode_compatible
class Document(models.Model):
    """
    """
    name = models.CharField(max_length=100)
    file = models.FileField(upload_to=upload_document_to, storage=document_file_storage, max_length=255)
    version = models.CharField(max_length=255)
    mime_type = models.CharField(max_length=255)
    document_type = models.ForeignKey('DocumentType')
    course = models.ForeignKey('Course')
    is_active = models.BooleanField(default=True)

    objects = ActiveManager()

    class Meta:
        ordering = ['course', 'name']
        db_table = 'ndptc_document'

    def __str__(self):
        return "%s - %s" % (self.name, self.document_type.name)

    def http_response(self):
        type = mimetypes.guess_extension(self.mime_type)
        response = HttpResponse(self.file.read(), mimetype=self.mime_type)
        response['Content-Length'] = self.file.size
        response['Content-Disposition'] = 'attachment; filename=' + self.name + type
        return response
reversion.register(Document)

@python_2_unicode_compatible
class DocumentDownload(models.Model):
    """
    """
    document = models.ForeignKey('Document')
    person = models.ForeignKey(DMSPerson)
    download_date = models.DateTimeField(default=timezone.now)

    class Meta:
        ordering = ['document', 'person']
        db_table = 'ndptc_document_download'


@python_2_unicode_compatible
class Photo(models.Model):
    item = models.ForeignKey('Question')
    title = models.CharField(max_length=100)
    image = models.ImageField(upload_to='tests/images')
    caption = models.CharField(max_length=250, blank=True, verbose_name='caption')

    class Meta:
        db_table = 'ndptc_photo'

    def __str__(self):
        return self.title
reversion.register(Photo)

@python_2_unicode_compatible
class TestType(models.Model):
    """
    """
    type = models.CharField(max_length=4, unique=True, primary_key=True)

    class Meta:
        db_table = 'ndptc_test_type'

    def __str__(self):
        return self.type




@python_2_unicode_compatible
class Test(models.Model):
    """
    """
    course = models.ForeignKey(Course)
    effective_date = models.DateField(default=timezone.now().date)
    type = models.ForeignKey(TestType)
    label = models.CharField(max_length=1000)
    is_active = models.BooleanField(default=True)

    objects = ActiveManager()

    class Meta:
        ordering = ['effective_date']
        db_table = 'ndptc_test'

    def has_data(self):
        if TestResult.objects.filter(question__test=self).exists():
            return True
        return False

    def __str__(self):
        return self.label
reversion.register(Test)


@python_2_unicode_compatible
class TrainingType(models.Model):
    """
    """
    code = models.CharField(max_length=2, unique=True)
    description = models.CharField(max_length=100)

    class Meta:
        ordering = ['description']
        db_table = 'ndptc_training_type'

    def __str__(self):
        return self.description


@python_2_unicode_compatible
class TrainingProvider(models.Model):
    """
    """
    email = models.EmailField(max_length=100)
    phone = models.CharField(max_length=10)
    provider = models.CharField(max_length=25, unique=True)

    class Meta:
        ordering = ['provider']
        db_table = 'ndptc_training_provider'

    def __str__(self):
        return self.provider


@python_2_unicode_compatible
class Question(models.Model):
    """
    """
    question = models.TextField()
    test = models.ForeignKey(Test)
    sort_order = models.IntegerField()
    #    ELO = models.ForeignKey('Course.ELO', null=True, blank=True)

    class Meta:
        ordering = ['sort_order']
        db_table = 'ndptc_question'

    def __str__(self):
        return self.question
reversion.register(Question)