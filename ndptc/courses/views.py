from django.contrib import messages
from django.shortcuts import get_object_or_404, redirect
from django.views.generic.base import TemplateView
from ndptc.accounts.mixins import LoginRequiredMixin
from ndptc.courses.models import Test


class ViewTestView(LoginRequiredMixin, TemplateView):
    def get_context_data(self, **kwargs):
        return {
            'test': self.test,
        }

    def get(self, request, pk, *args, **kwargs):
        has_permission, message = self.check_permission()
        if not has_permission:
            messages.error(request, message)
            return redirect(self.reverse_url())

        self.test = get_object_or_404(Test, pk=pk)
        return super(ViewTestView, self).get(request, *args, **kwargs)

    def check_permission(self):
        return True, None
