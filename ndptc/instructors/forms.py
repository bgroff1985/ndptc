from django import forms
from django.utils.datetime_safe import datetime
from ndptc.deliveries.models import Delivery
from ndptc.instructors.models import Certification, Availability
from .defs import *


#  TODO: This may actually belong in aku. We shall see if moi uses it.
from ndptc.utilities.widgets import RadioSelectNoList


class AvailabilityForm(forms.Form):
    def __init__(self, *args, **kwargs):
        self.instructor = kwargs.pop('instructor')
        super(AvailabilityForm, self).__init__(*args, **kwargs)

        today = datetime.utcnow().today()
        certifications = Certification.objects.filter(instructor=self.instructor)
        up_coming_deliveries = Delivery.objects.filter(start__gte=today) \
            .filter(course__id__in=certifications.values('course__id')).order_by('start').select_related()

        for delivery in up_coming_deliveries:
            id = str(delivery.id)
            try:
                Availability.objects.filter(instructor=self.instructor).get(delivery=delivery)
                self.fields[id] = forms.BooleanField(required=False, initial=True)
            except Availability.DoesNotExist:
                self.fields[id] = forms.BooleanField(required=False)
            self.fields[id].id = id
            self.fields[id].start = delivery.start
            self.fields[id].name = delivery.course
            self.fields[id].city_state = delivery.city_state()

    def save(self):
        for delivery_id in self.cleaned_data:
            available = self.cleaned_data[delivery_id]
            delivery = Delivery.objects.get(id=delivery_id)
            try:
                delivery = Availability.objects.filter(instructor=self.instructor).get(delivery=delivery)
                if not available:
                    delivery.delete()
            except Availability.DoesNotExist:
                if available:
                    Availability.objects.create(instructor=self.instructor, delivery=delivery)


