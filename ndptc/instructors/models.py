import reversion
from django.db import models
from ndptc.deliveries.defs import INSTRUCTOR_EVALUATION_QUESTIONS
from ndptc.managers.managers import ActiveManager


class EmploymentStatus(models.Model):
    status = models.CharField(max_length=20)
    code = models.CharField(max_length=2, unique=True)

    class Meta:
        db_table = 'ndptc_employment_status'

    def __str__(self):
        return self.status


class InstructorType(models.Model):
    description = models.CharField(max_length=20, unique=True)
    code = models.CharField(max_length=2, unique=True)

    class Meta:
        db_table = 'ndptc_instructor_type'

    def __str__(self):
        return self.description


class Availability(models.Model):
    instructor = models.ForeignKey('Instructor', on_delete=models.PROTECT, related_name='available_instructor')
    delivery = models.ForeignKey('deliveries.Delivery', related_name='available_delivery')

    class Meta:
        db_table = 'ndptc_availability'
reversion.register(Availability)


class Certification(models.Model):
    instructor = models.ForeignKey('Instructor', related_name='certifications_instructor')
    course = models.ForeignKey('courses.Course', related_name='certifications_course')
    subject_expert = models.BooleanField(default=False)
    emergency_management = models.BooleanField(default=False)
    date_certified = models.DateField(auto_now=True)

    is_active = models.BooleanField(default=True)

    objects = ActiveManager()

    class Meta:
        db_table = 'ndptc_certifications'
reversion.register(Certification)


class Taught(models.Model):
    instructor = models.ForeignKey('Instructor', on_delete=models.PROTECT, related_name='taught_instructor')
    instructor_type = models.ForeignKey('InstructorType', related_name='taught_instructor_type')
    delivery = models.ForeignKey('deliveries.Delivery', related_name='taught_delivery')

    is_active = models.BooleanField(default=True)

    objects = ActiveManager()

    class Meta:
        db_table = 'ndptc_taught'
reversion.register(Taught)


class Instructor(models.Model):
    person = models.ForeignKey('accounts.DMSPerson')
    employment_status = models.ForeignKey(EmploymentStatus, null=True, blank=True)
    effective_date = models.DateField(null=True, blank=True)
    background_check = models.BooleanField(default=False)
    background_date = models.DateField(null=True, blank=True)
    comments = models.TextField(null=True, blank=True)
    bio = models.TextField(null=True, blank=True)
    image = models.ImageField(upload_to='instructor_images/', null=True, blank=True)
    resume = models.FileField(upload_to='resume/')

    available = models.ManyToManyField('deliveries.Delivery', through=Availability,
                                       related_name='available_deliveries')
    certifications = models.ManyToManyField('courses.Course', through=Certification,
                                            related_name='instructor_certifications')
    taught = models.ManyToManyField('deliveries.Delivery', through=Taught, related_name='deliveries_taught')

    is_active = models.BooleanField(default=True)

    objects = ActiveManager()

    class Meta:
        db_table = 'ndptc_instructors'
        ordering = ('person__last_name', 'person__first_name')

    def evaluation_report(self):
        report = []
        for course in self.certifications.all():
            answers = [
                [0, 0, 0, 0, 0, 0, 0, INSTRUCTOR_EVALUATION_QUESTIONS[i]] for i in range(0, 6)
            ]
            comments = [[], INSTRUCTOR_EVALUATION_QUESTIONS[6]]
            for evaluation in self.instructorevaluation_set.filter(delivery__course=course):
                for i in range(0, 6):
                    answer = evaluation.__dict__['question{0}'.format(i + 1)]
                    if answer is not None:
                        answers[i][answer] += 1
                        # Number of responses
                        answers[i][6] += 1
                comments[0].append(evaluation.comment)
            report.append([course, {'answers': answers, 'comments': comments}])
        return report

    def __str__(self):
        return '{0}, {1}'.format(self.person.last_name, self.person.first_name)


reversion.register(Instructor)



