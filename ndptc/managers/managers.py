from django.db import models
from django.db.models.query import QuerySet
from django.utils.timezone import datetime, utc


class ActiveMixin(object):
    def active(self):
        return self.filter(is_active=True)

    def deleted(self):
        return self.filter(is_active=False)


class PassedMixin(object):
    def passed(self):
        return self.filter(attended=True, passed=True)

    def failed(self):
        return self.filter(attended=True, passed=False)

    def attended(self):
        return self.filter(attended=True)


class LatestMixin(object):
    def is_latest(self):
        return self.filter(is_latest=True)


class DeliveryFiscalYearMixin(object):
    def year(self, year):
        start = datetime(year - 1, 10, 1).replace(tzinfo=utc)
        end = datetime(year, 9, 30, 23, 59).replace(tzinfo=utc)
        return self.filter(start__gte=start, end__lte=end)


class ParticipantFiscalYearMixin(object):
    def year(self, year):
        start = datetime(year - 1, 10, 1).replace(tzinfo=utc)
        end = datetime(year, 9, 30, 23, 59).replace(tzinfo=utc)
        return self.filter(delivery__start__gte=start, delivery__end__lte=end)


class ActiveQuerySet(QuerySet, ActiveMixin):
    pass


class ActiveManager(models.Manager, ActiveMixin):
    def get_query_set(self):
        return ActiveQuerySet(self.model, using=self._db)


class DeliveryQuerySet(ActiveQuerySet, DeliveryFiscalYearMixin):
    pass


class ParticipantQuerySet(ActiveQuerySet, ParticipantFiscalYearMixin, PassedMixin):
    pass


class DeliveryManager(models.Manager, ActiveMixin, DeliveryFiscalYearMixin):
    def get_query_set(self):
        return DeliveryQuerySet(self.model, using=self._db)


class ParticipantManager(models.Manager, ActiveMixin, PassedMixin, ParticipantFiscalYearMixin):
    def get_query_set(self):
        return ParticipantQuerySet(self.model, using=self._db)


class LatestManager(ActiveManager, LatestMixin):
    pass
