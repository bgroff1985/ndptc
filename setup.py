from distutils.core import setup

setup(
    name='ndptc',
    version='0.1',
    url='https://ndptc.hawaii.edu/',
    author='Bryce Groff',
    author_email='bgroff@hawaii.edu',
    description=('Base models for the National Disaster'
                 'Preparedness Training Center.'),
    license='BSD',
    classifiers=[
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.2',
        'Programming Language :: Python :: 3.3',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
        'Topic :: Internet :: WWW/HTTP :: WSGI',
    ],
    packages = ['ndptc', 'ndptc.accounts', 'ndptc.courses', 'ndptc.deliveries', 'ndptc.instructors', 'ndptc.managers', 'ndptc.utilities']
)
